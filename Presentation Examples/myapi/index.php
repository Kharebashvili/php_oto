<?php
$request = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

if ($method == "OPTIONS") {
    // Handle CORS preflight request
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Authorization");
    exit(0);
}

switch ($request) {
    case '/users':
    case (preg_match('/\/users\/\d+/', $request) ? true : false):
        require __DIR__ . '/users.php';
        break;
    default:
        http_response_code(404);
        echo json_encode(array("message" => "Endpoint not found."));
        break;
}
?>

<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once 'database.php';

$database = new Database();
$db = $database->getConnection();

$request_method = $_SERVER["REQUEST_METHOD"];

switch($request_method) {
    case 'GET':
        if(!empty($_GET["id"])) {
            $id = intval($_GET["id"]);
            getUser($id);
        } else {
            getUsers();
        }
        break;
    case 'POST':
        addUser();
        break;
    case 'PUT':
        $id = intval($_GET["id"]);
        updateUser($id);
        break;
    case 'DELETE':
        $id = intval($_GET["id"]);
        deleteUser($id);
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}

function getUsers() {
    global $db;
    $query = "SELECT * FROM users";
    $stmt = $db->prepare($query);
    $stmt->execute();

    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($users);
}

function getUser($id) {
    global $db;
    $query = "SELECT * FROM users WHERE id = :id";
    $stmt = $db->prepare($query);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    echo json_encode($user);
}

function addUser() {
    global $db;
    $data = json_decode(file_get_contents("php://input"));

    $query = "INSERT INTO users (name, email) VALUES (:name, :email)";
    $stmt = $db->prepare($query);
    $stmt->bindParam(':name', $data->name);
    $stmt->bindParam(':email', $data->email);

    if($stmt->execute()) {
        echo json_encode(array('message' => 'User added successfully.'));
    } else {
        echo json_encode(array('message' => 'User could not be added.'));
    }
}

function updateUser($id) {
    global $db;
    $data = json_decode(file_get_contents("php://input"));

    $query = "UPDATE users SET name = :name, email = :email WHERE id = :id";
    $stmt = $db->prepare($query);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':name', $data->name);
    $stmt->bindParam(':email', $data->email);

    if($stmt->execute()) {
        echo json_encode(array('message' => 'User updated successfully.'));
    } else {
        echo json_encode(array('message' => 'User could not be updated.'));
    }
}

function deleteUser($id) {
    global $db;

    $query = "DELETE FROM users WHERE id = :id";
    $stmt = $db->prepare($query);
    $stmt->bindParam(':id', $id);

    if($stmt->execute()) {
        echo json_encode(array('message' => 'User deleted successfully.'));
    } else {
        echo json_encode(array('message' => 'User could not be deleted.'));
    }
}
?>

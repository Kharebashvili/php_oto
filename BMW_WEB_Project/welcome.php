<?php
session_start();

$status = $_GET['status'] ?? '';

if ($status == 'registered') {
    $message = "Registration successful! You can now log in.";
} elseif ($status == 'loggedin') {
    $message = "Login successful! Welcome, " . $_SESSION['username'] . ".";
} else {
    $message = "Welcome!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .container {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: center;
        }
        h2 {
            margin-bottom: 20px;
        }
        a {
            display: inline-block;
            margin-top: 20px;
            padding: 10px 20px;
            background-color: #007BFF;
            color: #fff;
            border-radius: 4px;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2><?php echo $message; ?></h2>
        <?php if ($status == 'registered'): ?>
            <a href="login.php">Login</a>
        <?php elseif ($status == 'loggedin'): ?>
            <a href="logout.php">Logout</a>
        <?php endif; ?>
    </div>
</body>
</html>

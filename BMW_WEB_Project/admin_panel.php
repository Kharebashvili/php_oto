<?php
session_start();

if (!isset($_SESSION['admin_id'])) {
    header("Location: admin_login.php");
    exit();
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="Styles/style.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        header {
            background-color: #333;
            color: #fff;
            padding: 20px;
            text-align: center;
        }
        .user-info {
            position: absolute;
            top: 20px;
            right: 20px;
        }
        .user-info p {
            display: inline-block;
            margin: 0 10px 0 0;
        }
        .user-info a {
            color: #fff;
            text-decoration: none;
        }
        nav {
            background-color: #555;
            overflow: hidden;
        }
        nav a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 14px 20px;
            text-decoration: none;
        }
        nav a:hover {
            background-color: #ddd;
            color: #000;
        }
        .container {
            padding: 20px;
        }
        .admin-section {
            background-color: #fff;
            padding: 20px;
            margin-bottom: 20px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .admin-section h2 {
            margin-top: 0;
        }
        .admin-section form {
            margin-top: 10px;
        }
        .admin-section label {
            display: block;
            margin-bottom: 5px;
        }
        .admin-section input[type="text"],
        .admin-section input[type="password"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
            box-sizing: border-box;
        }
        .admin-section button {
            padding: 10px 20px;
            background-color: #333;
            color: #fff;
            border: none;
            cursor: pointer;
        }
        .admin-section button:hover {
            background-color: #555;
        }
        footer {
            text-align: center;
            padding: 20px;
            background-color: #333;
            color: #fff;
        }
    </style>
</head>
<body>
    <header>
        <h1>Admin Panel</h1>
        <div class="user-info">
            <p>Welcome, <?php echo htmlspecialchars($_SESSION['admin_id']); ?>!</p>
            <a href="admin_logout.php">Logout</a>
        </div>
    </header>
    <nav>
    <nav>
        <a href="admin_panel.php">Dashboard</a>
        <a href="admin_edit_models.php">Manage Models</a>
        <a href="admin_manage_users.php">Manage Users</a>
        <a href="admin_add_user.php">Add User</a> <!-- New link for adding users -->
    </nav>
    </nav>
    <div class="container">
        <div class="admin-section">
            <h2>Edit Models</h2>
            <form method="post" action="admin_edit_models.php">
                <label for="model_name">Model Name:</label>
                <input type="text" id="model_name" name="model_name" required>
                <label for="model_description">Model Description:</label>
                <textarea id="model_description" name="model_description" rows="4" required></textarea>
                <button type="submit">Save Changes</button>
            </form>
        </div>
    </div>
    <footer>
        <p>&copy; 2024 BMW Information Website</p>
    </footer>
</body>
</html>

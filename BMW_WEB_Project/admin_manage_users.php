<!-- admin_manage_users.php -->
<?php
session_start();

if (!isset($_SESSION['admin_id'])) {
    header("Location: admin_login.php");
    exit();
}

require 'config.php';

// Fetch all users
$sql = "SELECT id, username, email FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $users = $result->fetch_all(MYSQLI_ASSOC);
} else {
    $users = [];
}

$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Users</title>
    <link rel="stylesheet" href="Styles/style.css">
    <style>
        /* Add your styles as needed */
    </style>
</head>
<body>
    <header>
        <h1>Manage Users</h1>
        <div class="user-info">
            <p>Welcome, <?php echo htmlspecialchars($_SESSION['admin_id']); ?>!</p>
            <a href="admin_logout.php">Logout</a>
        </div>
    </header>
    <nav>
        <a href="admin_panel.php">Dashboard</a>
        <a href="admin_edit_models.php">Manage Models</a>
        <a href="admin_manage_users.php">Manage Users</a>
        <a href="admin_add_user.php">Add User</a>
    </nav>
    <div class="container">
        <h2>User List</h2>
        <?php if (!empty($users)): ?>
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?php echo $user['id']; ?></td>
                            <td><?php echo htmlspecialchars($user['username']); ?></td>
                            <td><?php echo htmlspecialchars($user['email']); ?></td>
                            <td>
                                <a href="admin_edit_user.php?id=<?php echo $user['id']; ?>">Edit</a> |
                                <a href="admin_delete_user.php?id=<?php echo $user['id']; ?>">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <p>No users found.</p>
        <?php endif; ?>
    </div>
    <footer>
        <p>&copy; 2024 BMW Information Website</p>
    </footer>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BMW Information Website</title>
    <link rel="stylesheet" href="Styles/style.css">
</head>
<body>
    <header>
        <h1>Welcome to the BMW Georgia</h1>
        
    </header>
    <nav>
        <a href="#models">Models</a>
        <a href="#news">News</a>
        <a href="#contact">Contact Us</a>
        <a href="register.php">Sign Up</a>
        <a href="login.php">Log In As User</a>
        <a href="admin_login.php">Log In As Admin</a>
    </nav>
    <div class="container">
        <section id="models">
    <div class="slider">
        <div class="slides">
            <div class="slide"><img src="https://wallpapercave.com/wp/wp4258095.jpg" alt="BMW 3 Series"></div>
            <div class="slide"><img src="https://wallpapercave.com/wp/wp4262062.jpg" alt="BMW 8 Series"></div>
            <div class="slide"><img src="https://imgd.aeplcdn.com/1920x1080/n/cw/ec/96443/5-series-exterior-left-front-three-quarter.jpeg?q=80&q=80" alt="BMW 5 Series"></div>
            <div class="slide"><img src="https://images6.alphacoders.com/880/880915.jpg" alt="BMW 7 Series"></div>
        </div>
        <div class="slider-buttons">
            <button class="slider-button" onclick="prevSlide()">&#10094;</button>
            <button class="slider-button" onclick="nextSlide()">&#10095;</button>
        </div>
    </div>
    <h2>Models</h2>
    <div class="models">
        <div class="model">
            <a href="model_x1.html">
                <img src="https://www.bmw-georgia.com/content/dam/bmw/common/all-models/m-series/x1-m35i/2023/highlights/bmw-x1-mp-gallery-impressions-01_890.jpg" alt="BMW X1">
                <h3>BMW X1</h3>
                <p>The BMW X1 is a line of subcompact luxury crossover SUVs produced by BMW.</p>
            </a>
        </div>
        <div class="model">
            <a href="model_x3.html">
                <img src="https://media.ed.edmunds-media.com/bmw/x3/2022/oem/2022_bmw_x3_4dr-suv_xdrive30i_fq_oem_1_1600.jpg" alt="BMW X3">
                <h3>BMW X3</h3>
                <p>The BMW X3 is a compact luxury crossover SUV manufactured by German automaker BMW.</p>
            </a>
        </div>
        <div class="model">
            <a href="model_x5.html">
                <img src="https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X5-2023/10452/1688992642182/front-left-side-47.jpg" alt="BMW X5">
                <h3>BMW X5</h3>
                <p>The BMW X5 is a mid-size luxury crossover SUV produced by BMW.</p>
            </a>
        </div>
    </div>
</section>

        <section id="news">
            <h2>Latest News</h2>
            <div class="news">
                <div class="news-item">
                    <img src="https://www.topgear.com/sites/default/files/images/news-article/2020/11/08045f839300192093d0912f5e733f7f/the_first_bmw_ix_1.jpg" alt="BMW News 1">
                    <h3>BMW Announces New Electric Vehicle</h3>
                    <p>BMW has announced the release of a new electric vehicle, set to revolutionize the industry.</p>
                </div>
                <div class="news-item">
                    <img src="https://cdn.bmwblog.com/wp-content/uploads/2024/02/2024-bmw-i5-m60-touring-05-1.jpg" alt="BMW News 2">
                    <h3>BMW Wins Car of the Year</h3>
                    <p>The BMW 5 Series has been awarded the prestigious Car of the Year award for 2024.</p>
                </div>
                <div class="news-item">
                    <img src="https://mediapool.bmwgroup.com/cache/P9/202003/P90386217/P90386217-oliver-zipse-chairman-of-the-board-of-management-of-bmw-ag-annual-accounts-press-conference-at-bmw-w-2375px.jpg" alt="BMW News 3">
                    <h3>New BMW Technology Innovations</h3>
                    <p>BMW is leading the way in automotive technology with its latest innovations.</p>
                </div>
            </div>
        </section>
        <section id="contact">
    <h2>Contact Us</h2>
    <form action="save_message.php" method="post" id="contactForm">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" required>
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required>
        <label for="message">Message:</label>
        <textarea id="message" name="message" required></textarea>
        <input type="submit" value="Submit">
    </form>
</section>
    </div>
    <footer>
        <p>&copy; 2024 BMW Georgia</p>
    </footer>
    <script>
        let currentSlide = 0;
        const slides = document.querySelectorAll('.slide');
        
        function showSlide(index) {
            if (index >= slides.length) {
                currentSlide = 0;
            } else if (index < 0) {
                currentSlide = slides.length - 1;
            } else {
                currentSlide = index;
            }
            const offset = -currentSlide * 100;
            document.querySelector('.slides').style.transform = `translateX(${offset}%)`;
        }

        function nextSlide() {
            showSlide(currentSlide + 1);
        }

        function prevSlide() {
            showSlide(currentSlide - 1);
        }

        setInterval(nextSlide, 3500);
        
        document.getElementById('contactForm').addEventListener('submit', function(event) {
            event.preventDefault();
            alert('Thank you for your message!');
            this.submit();
        });
    </script>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = htmlspecialchars($_POST['name']);
        $email = htmlspecialchars($_POST['email']);
        $message = htmlspecialchars($_POST['message']);
        
        
        echo "<script>alert('Thank you, $name. We have received your message.');</script>";
    }


    session_start();
    $isLoggedIn = isset($_SESSION['user_id']);
    $username = $isLoggedIn ? $_SESSION['username'] : '';


    ?>
</body>
</html>

<?php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BMW X3</title>
    <link rel="stylesheet" href="Styles/style.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        header {
            background-color: #333;
            color: #fff;
            padding: 20px;
            text-align: center;
        }
        .user-info {
            position: absolute;
            top: 20px;
            right: 20px;
        }
        .user-info p {
            display: inline-block;
            margin: 0 10px 0 0;
        }
        .user-info a {
            color: #fff;
            text-decoration: none;
        }
        nav {
            background-color: #555;
            overflow: hidden;
        }
        nav a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 14px 20px;
            text-decoration: none;
        }
        nav a:hover {
            background-color: #ddd;
            color: #000;
        }
        .container {
            padding: 20px;
        }
        .model-detail {
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .model-detail img {
            max-width: 100%;
            height: auto;
        }
        .model-detail h2 {
            margin: 10px 0;
        }
        .model-detail p {
            margin: 10px 0;
        }
        footer {
            text-align: center;
            padding: 20px;
            background-color: #333;
            color: #fff;
        }
    </style>
</head>
<body>
    <header>
        <h1>BMW X3</h1>
        <div class="user-info">
            <?php if (isset($_SESSION['username'])): ?>
                <p>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?>!</p>
                <a href="logout.php">Logout</a>
            <?php endif; ?>
        </div>
    </header>
    <nav>
        <a href="models.php">Models</a>
    </nav>
    <div class="container">
        <div class="model-details">
            <img src="https://media.ed.edmunds-media.com/bmw/x3/2022/oem/2022_bmw_x3_4dr-suv_xdrive30i_fq_oem_1_1600.jpg" alt="BMW X1">
            <h2>BMW X3</h2>
            <p>The BMW X3 is a subcompact luxury crossover SUV manufactured by BMW. Below are the specifications:</p>
            <ul>
                <li>Price: 30,000$</li>
                <li>Engine Size: 2.0L I4</li>
                <li>Transmission: 8-speed automatic</li>
                <li>Colors: Alpine White, Black Sapphire, Mineral White, Glacier Silver, etc.</li>
                <li>Features: Navigation, Leather seats, Panoramic sunroof, Adaptive cruise control, etc.</li>
            </ul>
        </div>
    </div>
    <footer>
        <p>&copy; 2024 BMW Information Website</p>
    </footer>
<!-- admin_add_user.php -->
<?php
session_start();

if (!isset($_SESSION['admin_id'])) {
    header("Location: admin_login.php");
    exit();
}

require 'config.php';

$username = $email = $password = '';
$username_err = $email_err = $password_err = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Validate username
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter a username.";
    } else {
        $username = trim($_POST["username"]);
    }

    // Validate email
    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter an email address.";
    } else {
        $email = trim($_POST["email"]);
    }

    // Validate password
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter a password.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Check input errors before inserting into database
    if (empty($username_err) && empty($email_err) && empty($password_err)) {
        // Hash password
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        // Insert user into database
        $sql = "INSERT INTO users (username, email, password) VALUES (?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sss", $username, $email, $hashed_password);

        if ($stmt->execute()) {
            header("Location: admin_manage_users.php");
            exit();
        } else {
            echo "Error inserting user.";
        }

        $stmt->close();
    }
    
    $conn->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add User</title>
    <link rel="stylesheet" href="Styles/style.css">
    <style>
        /* Add your styles as needed */
    </style>
</head>
<body>
    <header>
        <h1>Add User</h1>
        <div class="user-info">
            <p>Welcome, <?php echo htmlspecialchars($_SESSION['admin_id']); ?>!</p>
            <a href="admin_logout.php">Logout</a>
        </div>
    </header>
    <nav>
        <a href="admin_panel.php">Dashboard</a>
        <a href="admin_edit_models.php">Manage Models</a>
        <a href="admin_manage_users.php">Manage Users</a>
        <a href="admin_add_user.php">Add User</a>
    </nav>
    <div class="container">
        <h2>Add New User</h2>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" value="<?php echo $username; ?>" required>
            <span class="error"><?php echo $username_err; ?></span>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" value="<?php echo $email; ?>" required>
            <span class="error"><?php echo $email_err; ?></span>
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" value="<?php echo $password; ?>" required>
            <span class="error"><?php echo $password_err; ?></span>
            <button type="submit">Add User</button>
        </form>
    </div>
    <footer>
        <p>&copy; 2024 BMW Information Website</p>
    </footer>
</body>
</html>

<?php
// models.php
session_start();

if (!isset($_SESSION['user_id'])) {
    header("Location: login.php");
    exit();
}

require 'config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['model'])) {
    $userId = $_SESSION['user_id'];
    $model = htmlspecialchars($_POST['model']);
    
    $stmt = $conn->prepare("INSERT INTO orders (user_id, model) VALUES (?, ?)");
    $stmt->bind_param("is", $userId, $model);
    $stmt->execute();
    $stmt->close();
    
    $message = "Order placed successfully for $model.";
}

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BMW Models</title>
    <link rel="stylesheet" href="Styles/style.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        header {
            background-color: #333;
            color: #fff;
            padding: 20px;
            text-align: center;
        }
        .user-info {
            position: absolute;
            top: 20px;
            right: 20px;
        }
        .user-info p {
            display: inline-block;
            margin: 0 10px 0 0;
        }
        .user-info a {
            color: #fff;
            text-decoration: none;
        }
        nav {
            background-color: #555;
            overflow: hidden;
        }
        nav a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 14px 20px;
            text-decoration: none;
        }
        nav a:hover {
            background-color: #ddd;
            color: #000;
        }
        .container {
            padding: 20px;
        }
        .models {
            display: flex;
            flex-wrap: wrap;
        }
        .model {
            flex: 1 1 30%;
            margin: 10px;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
            text-align: center;
        }
        .model img {
            max-width: 100%;
            height: auto;
        }
        .model h3 {
            margin: 10px 0;
        }
        .model form {
            margin-top: 10px;
        }
        footer {
            text-align: center;
            padding: 20px;
            background-color: #333;
            color: #fff;
        }
    </style>
</head>
<body>
    <header>
        <h1>BMW Models</h1>
        <div class="user-info">
            <?php if (isset($_SESSION['username'])): ?>
                <p>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?>!</p>
                <a href="logout.php">Logout</a>
            <?php endif; ?>
        </div>
    </header>
    <nav>
        <a href="models.php">Models</a>
    </nav>
    <div class="container">
        <h2>Available BMW Models</h2>
        <?php if (isset($message)): ?>
            <p><?php echo $message; ?></p>
        <?php endif; ?>
        <div class="models">
            <div class="model">
                <h3>BMW X1</h3>
                <a href="model_x1.php"><img src="https://www.bmw-georgia.com/content/dam/bmw/common/all-models/m-series/x1-m35i/2023/highlights/bmw-x1-mp-gallery-impressions-01_890.jpg" alt="BMW X1"></a>
                <form method="post" action="models.php">
                    <input type="hidden" name="model" value="BMW X1">
                    <button type="submit">Order BMW X1</button>
                </form>
            </div>
            <div class="model">
                <h3>BMW X3</h3>
                <a href="model_x3.php"><img src="https://media.ed.edmunds-media.com/bmw/x3/2022/oem/2022_bmw_x3_4dr-suv_xdrive30i_fq_oem_1_1600.jpg" alt="BMW X3"></a>
                <form method="post" action="models.php">
                    <input type="hidden" name="model" value="BMW X3">
                    <button type="submit">Order BMW X3</button>
                </form>
            </div>
            <div class="model">
                <h3>BMW X5</h3>
                <a href="model_x5.php"><img src="https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X5-2023/10452/1688992642182/front-left-side-47.jpg" alt="BMW X5"></a>
                <form method="post" action="models.php">
                    <input type="hidden" name="model" value="BMW X5">
                    <button type="submit">Order BMW X5</button>
                </form>
            </div>
        </div>
    </div>
    <footer>
        <p>&copy; 2024 BMW Information Website</p>
    </footer>
</body>
</html>
